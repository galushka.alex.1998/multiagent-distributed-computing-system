import dotenv from 'dotenv';
import { SequenceConstructor } from './constructors/SequenceConstructor';
import { TimeSeriesConstructor } from './constructors/TimeSeriesConstructor';
import { LSys } from './cores/gmatic/LSys';
import { LSysConfig } from './cores/gmatic/LSysConfig';
import { LSysDescription } from './cores/gmatic/LSysDescription';
import { Rule } from './cores/gmatic/Rule';
import { GNeticConfig, GNeticHandle } from './cores/gnetic/GNeticHandle';

dotenv.config()

async function run(){
  for (let i = 0; i < 100; i++){
    await new Promise(resolve => {


      const lsysConfig = new LSysConfig();
      const f = new Rule('f', 'f-f++f-f');
      const lsysDefinition = new LSysDescription('f', [f]);
      const lsys = LSys.fromDescription(lsysDefinition, lsysConfig);

      const sequence = new SequenceConstructor().setConfiguration({
        lSystem: lsys,
        symbolsCount: 64
      }).build();

      const timeSeries = new TimeSeriesConstructor().setConfiguration({
        sequence,
        value: 25,
        dValue: 5
      }).build();

      let gNeticConfig: GNeticConfig = new GNeticConfig();
      gNeticConfig.timeSeries = timeSeries;
      gNeticConfig.crossoveringCount = 10;
      gNeticConfig.mutationCount = 10;
      gNeticConfig.surviveCount = 20;
      gNeticConfig.accuracy = 1;


      const handle = new GNeticHandle();

      const startTime = new Date();
      handle.run(gNeticConfig);

      // const interval = setInterval(async () => {
      //   const results = await handle.getCurrentResults();
      //   console.clear();
      //   console.log(results[0]);
      // }, 100);

      handle.onSolutionFound((solution) => {
        //clearInterval(interval);
        //console.log('SOLUTION FOUND');
        console.log(solution);
        const finishTime = new Date();

        console.log(`Time ${i} : ${(finishTime.getTime() - startTime.getTime()) / 1000} s`)
        resolve(null);
      });
    })
  }
}

run();



