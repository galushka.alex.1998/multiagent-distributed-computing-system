

export class PyLike {
  static normalize(x: number, xMin: number, xMax: number, dMin: number, dMax: number): number{
    return ((x - xMin) * (dMax - dMin)) / (xMax - xMin) + dMin;
  }
  static shuffle(arr: Array<any>): Array<any>{
    return arr.sort(() => 0.5 - Math.random());
  }
  static xrange(count: number): Array<any>{
    return new Array(count).fill(null);
  }
  static rand(min: number, max: number): number{
    return Math.random() * (max - min) + min;
  }
  static randInt(min: number, max: number): number{
    return Math.round(PyLike.rand(min, max));
  }
  static randItem(arr: Array<any>): any{
    return arr[PyLike.randInt(0, arr.length - 1)];
  }
  static take(arr: Array<any>, count: number): Array<any>{
    return arr.filter((_, idx) => idx < count);
  }
}