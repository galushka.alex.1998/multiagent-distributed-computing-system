

export enum AgentType {
  CommonClusterAgent = 'CommonClusterAgent',
  WorkerAgent = 'WorkerAgent'
}
