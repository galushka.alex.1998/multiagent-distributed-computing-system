import { Rule } from "./Rule";


export class LSysDescription {
  public axiom: string;
  public rules: Array<Rule> = [];
  constructor(axiom: string = "", rules: Array<Rule> = []) {
    this.axiom = axiom;
    this.rules = rules;
  }
}