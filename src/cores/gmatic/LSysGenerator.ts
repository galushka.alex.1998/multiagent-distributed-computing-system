import { PyLike } from "../../helpers/pylike";
import { LSys } from "./LSys";
import { LSysConfig } from "./LSysConfig";
import { Rule } from "./Rule";

export class LSysGenerator {
  static generateRandomLSystem(){
    const config = new LSysConfig();
    let lsys = null;
    do {
      const ruleCount = 1;
      const axiomLength = 1; 
      let freeSymbols = PyLike.take(config.significant, ruleCount);
  
      const axiom = PyLike.xrange(axiomLength)
        .map(() => PyLike.randItem(freeSymbols))
        .join("");
      const rules = PyLike.xrange(ruleCount).map((_, idx) => {
        const ruleAxiom = freeSymbols[idx];
        const ruleLength = PyLike.randInt(3, 15);
        const rule = this.generateRandomRule(
          ruleAxiom,
          [...freeSymbols,
          ...config.functors, ''],
          ruleLength
        );
        return rule;
      });
      lsys = new LSys(config);
      lsys.addAxiom(axiom);
      rules.forEach((rule) => {
        lsys.addRule(rule);
      });
    } while (!lsys.isSystemValid());
  
    return lsys;
  };

  static generateRandomRule(start, literals, size){
    let production = "";
    do {
      production = PyLike.xrange(size)
        .map(() => PyLike.randItem(literals))
        .join("");
    } while (Array.from(production).filter((s) => s === "f").length < 2);

    const unnecessaryStatements = ["+-", "-+"];
    while (unnecessaryStatements.some((statement) => production.includes(statement))) {
      unnecessaryStatements.forEach((statement) => {
        production = production.replace(
          statement,
          PyLike.xrange(statement.length)
            .map(() => PyLike.randItem(literals))
            .join("")
        );
      });
    }
    return new Rule(start, production);
  }
}

