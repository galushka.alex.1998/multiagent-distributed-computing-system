export class LSysConfig {
  public start: string = 'S';
  public significant: Array<string> = ['f'];
  public functors: Array<string> = ['+', '-'];
}