

export class Rule {
  public axiom: string;
  public production: string;

  constructor(axiom: string, production: string){
    this.axiom = axiom;
    this.production = production;
  }

  toString(){
    return `${this.axiom}=>${this.production}`;
  }
}