import { LSysConfig } from "./LSysConfig";
import { Rule } from "./Rule";
import { LSysDescription } from './LSysDescription'



class LSysNode {
  public axiom: string;
  public relatedNodes: Array<LSysNode>;
  public isVisited: boolean;
  constructor(axiom: string) {
    this.axiom = axiom;
    this.relatedNodes = [];
    this.isVisited = false;
  }
  public isRelatedWith(axiom: string) {
    if (this.isVisited) {
      return false;
    }
    this.isVisited = true;
    const target: LSysNode =
      this.axiom !== axiom
        ? this.relatedNodes.find(
            (node) => node.axiom === axiom || node.isRelatedWith(axiom)
          )
        : this;
    return !!target;
  }
}

export class LSys {
  public config: LSysConfig = null;
  public axiomRule: Rule = null;
  public rules: Array<Rule> = [];
  constructor(config: LSysConfig) {
    this.config = config;
    this.axiomRule = null;
    this.rules = [];
  }
  public addAxiom(axiom: string): void {
    this.axiomRule = new Rule(this.config.start, axiom);
  }

  public addRule(rule: Rule): void {
    this.verifyRuleValid(rule);
    this.rules.push(rule);
  }

  public verifyRuleValid(rule: Rule) {
    if (!this.config.significant.some((v) => v === rule.axiom)) {
      throw new Error();
    }
  }

  public buildGraph(): LSysNode {
    const nodes = [
      this.config.start,
      ...this.config.significant,
      ...this.config.functors,
    ].map((nodeAxiom) => new LSysNode(nodeAxiom));

    const rules = [this.axiomRule, ...this.rules];
    rules.forEach((rule) => {
      const node = nodes.find((node) => node.axiom === rule.axiom);
      Array.from(rule.production).forEach((symbol) => {
        const child = nodes.find(
          (n) =>
            n.axiom === symbol &&
            !node.relatedNodes.some((cn) => cn.axiom === n.axiom)
        );
        if (child) {
          node.relatedNodes.push(child);
        }
      });
    });

    return nodes[0];
  }

  public isSystemValid(): boolean {
    let graph = this.buildGraph();
    const literals = [...this.config.significant, ...this.config.functors];
    const rulesContactivityResult = literals.every((literal) => {
      let result = graph.isRelatedWith(literal);
      graph = this.buildGraph();
      return result;
    });

    let isReproducing = true;
    let start = this.axiomRule.production;
    for (let i = 0; i < this.rules.length + 1; i++) {
      let iterationResult = this.iterate(start);

      const significantIteration = Array.from(iterationResult)
        .filter((s) => this.config.significant.some((symbol) => symbol === s))
        .join("");
      const significantStart = Array.from(start)
        .filter((s) => this.config.significant.some((symbol) => symbol === s))
        .join("");
      if (significantIteration.length <= significantStart.length) {
        isReproducing = false;
        break;
      }
      start = iterationResult;
    }

    return rulesContactivityResult && isReproducing;
  }

  public iterate(start: string = ""): string {
    let axiom = start ? start : this.axiomRule.production;
    let iterationResult = "";
    Array.from(axiom).forEach((symbol) => {
      const targetRule = this.rules.find((r) => r.axiom === symbol);
      iterationResult =
        iterationResult + (targetRule ? targetRule.production : symbol);
    });
    return iterationResult;
  }

  iterateTimes(count: number): string {
    let start = this.axiomRule.production;
    for (let i = 0; i < count; i++) {
      start = this.iterate(start);
    }
    return start;
  }

  public static fromDescription(description: LSysDescription, config: LSysConfig): LSys {
    if (!description.axiom || !description.rules) {
      throw new Error("Description is not valid!");
    }
    const lsys = new LSys(config);
    lsys.addAxiom(description.axiom);
    description.rules.forEach((rule) => lsys.addRule(rule));
    return lsys;
  }

  public toDescription(): LSysDescription {
    return new LSysDescription(
      this.axiomRule.production,
      this.rules.map((r) => new Rule(r.axiom, r.production))
    );
  }

  public deepcopy(): LSys {
    return LSys.fromDescription(this.toDescription(), this.config);
  }

  public toString(): string {
    return `${this.axiomRule.production}  ${this.rules.map((r) =>
      r.toString()
    )}`;
  }
}