import Axios, { AxiosRequestConfig, AxiosResponse } from 'axios';

export interface IHttpClient {
  post<TResponse>(baseUrl: string, url: string, params: any): Promise<AxiosResponse<TResponse>>;
  get<TResponse>(baseUrl: string, url: string, params: any): Promise<AxiosResponse<TResponse>>;
  put<TResponse>(baseUrl: string, url: string, params: any): Promise<AxiosResponse<TResponse>>;
  delete<TResponse>(baseUrl: string, url: string, params: any): Promise<AxiosResponse<TResponse>>;
}

export class HttpClientBase implements IHttpClient {
  private createAxiosConfig(baseUrl, url, method, params): AxiosRequestConfig{
    const config: AxiosRequestConfig = {
      baseURL: baseUrl,
      url: url,
      params: params
    }
    return config;
  }
  private async request<TResponse>(baseUrl: string, url: string, method: string, params: any): Promise<AxiosResponse<TResponse>>{
    const config = this.createAxiosConfig(baseUrl, url, method, params);
    return await Axios.request(config);
  }
  public async post<TResponse> (baseUrl: string, url: string, params: any): Promise<AxiosResponse<TResponse>> {
    return await this.request(baseUrl, url, 'POST', params);
  }
  public async get<TResponse>(baseUrl: string, url: string): Promise<AxiosResponse<TResponse>> {
    return await this.request(baseUrl, url, 'GET', null);
  }
  public async put<TResponse>(baseUrl: string, url: string, params: any): Promise<AxiosResponse<TResponse>> {
    return await this.request(baseUrl, url, 'PUT', params);
  }
  public async delete<TResponse>(baseUrl: string, url: string): Promise<AxiosResponse<TResponse>> {
    return await this.request(baseUrl, url, 'DELETE', null);
  }
}

