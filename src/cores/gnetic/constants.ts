
export enum GNeticMessageTypes {
  BEGIN = 'BEGIN',
  STOP = 'STOP',
  GIVE_INTERMEDIATE_RESULTS = 'GIVE_INTERMEDIATE_RESULTS',
  SOLUTION_FOUND = 'SOLUTION_FOUND'
} 

