import { parentPort } from 'worker_threads';
import { SequenceConstructor } from '../../constructors/SequenceConstructor';
import { TimeSeriesConstructor } from '../../constructors/TimeSeriesConstructor';
import { PyLike } from '../../helpers/pylike';
import { LSys } from '../gmatic/LSys';
import { LSysGenerator } from '../gmatic/LSysGenerator';
import { GNeticConfig } from './GNeticHandle';
import _ from 'lodash';
import { buildWorkerParentEndpoint } from '../messages/worker_threads/Threads';
import { GNeticMessageTypes } from './constants';
import { AsyncMessage, TypedMessage } from '../messages/AsyncMessage';




class GNetic {
  private settings: GNeticConfig = null;
  private population: Array<Chromosome> = [];
  private iterationsCount: number = 0;
  private isRunning: boolean = false;
  private solutionFoundCallback: Function = () => null;
  constructor (config: GNeticConfig){
    this.settings = config;
    this.process = this.process.bind(this);
  }

  public run(){
    this.isRunning = true;
    this.population = PyLike.xrange(
      this.settings.surviveCount
    ).map(() =>
      new Chromosome(() => this.settings.timeSeries).initbase()
    );
    this.process();
  }

  public stop(){
    this.isRunning = false;
  }

  public getBestChromosomes (){
    return PyLike.take(this.population.sort((a, b) => a.fitness - b.fitness), 10).map((c: Chromosome) => {
      return {
        lsys: c.lSystem.toString(),
        value: c.value,
        dValue: c.dValue
      }
    });
  }

  public onSolutionFound(callback: ({ lsys, value, dValue}) => void){
    this.solutionFoundCallback = callback;
  }

  public process(){
    this.rebasePhase();
    this.crossoverPhase();
    this.mutationPhase();
    this.selectionPhase();
    if (this.isRunning){
      setImmediate(this.process);
    }
  }

  public selectionPhase(){
    const sorted = this.population.sort((a, b) => a.fitness - b.fitness);
    this.population = PyLike.take(sorted, this.settings.surviveCount);
    this.iterationsCount++;
    this.population.forEach((c) => c.age++);

    if (this.population[0].fitness <= this.settings.accuracy){
      if (this.solutionFoundCallback){
        const solution = this.population[0];
        this.solutionFoundCallback({ 
          lsys: solution.lSystem.toString(), 
          value: solution.value, 
          dValue: solution.dValue 
        });
        this.isRunning = false;
      }
    }
  }
  public crossoverPhase(){
    const crossoveringChromosomes: Array<Chromosome> = PyLike.xrange(
      this.settings.crossoveringCount
    ).map(() => this.population[PyLike.randInt(0, this.population.length - 1)]);

    const children: Array<Chromosome> = PyLike.xrange(this.settings.crossoveringCount)
      .map(() => {
        const mom: Chromosome = PyLike.randItem(crossoveringChromosomes);
        const dad: Chromosome = PyLike.randItem(crossoveringChromosomes);
        return mom.crossover(dad);
      })
      .reduce((acc, curr) => [...acc, ...curr]);

    this.population = [...this.population, ...children];
  }
  public mutationPhase(){
    const mutants = PyLike.xrange(this.settings.mutationCount).map(() =>
      this.population[PyLike.randInt(0, this.population.length - 1)].mutate()
    );
    this.population = [...this.population, ...mutants].sort(
      (a, b) => a.fitness - b.fitness
    );
  }
  public rebasePhase(){
    if (
      !this.population.find((c) => c.fitness === 0) &&
      this.population.filter((c) => c.age > 100).length >= this.settings.surviveCount
    ) {
      this.population = [
        this.population.sort((a, b) => a.fitness - b.fitness)[0],
        ...PyLike.xrange(this.settings.surviveCount).map(() =>
          new Chromosome(() => this.settings.timeSeries).initbase()
        ),
      ];
    }
  }
  public cloneEliminationPhase(){
    let uniqPopulation = _.uniqBy(this.population, (chromosome: Chromosome) =>
      chromosome.toString()
    );

    if (uniqPopulation.length < this.population.length) {
      const diff = this.population.length - uniqPopulation.length;
      const newChromosomes = PyLike.xrange(diff).map(() =>
        new Chromosome(() => this.settings.timeSeries).initbase()
      );
      this.population = [...uniqPopulation, ...newChromosomes];
    }
  }
}

class Chromosome {
  public lSystem: LSys = null;
  public value: number = 0;
  public dValue: number = 0;
  public age: number = 0;
  public fitness: number = 0;
  public getTimeSeries: () => Array<number> = () => [];

  constructor(getTimeSeries){
    this.getTimeSeries = getTimeSeries;
  }

  initbase(): Chromosome {
    this.lSystem = LSysGenerator.generateRandomLSystem();
    this.value = PyLike.rand(0, 1000);
    this.dValue = PyLike.rand(0, 100);
    this.calculateFitness();
    return this;
  }

  mutate(): Chromosome {
    const randGen = PyLike.randInt(0, 2);
    const chromosome = this.deepcopy();
    switch (randGen) {
      case 0:
        chromosome.lSystem = LSysGenerator.generateRandomLSystem();
        break;
      case 1:
        chromosome.value = PyLike.rand(0, 100);
        break;
      case 2:
        chromosome.dValue = PyLike.rand(0, 100);
        break;
    }
    chromosome.calculateFitness();
    return chromosome;
  }

  crossover(other: Chromosome): Chromosome[] {

    const first = new Chromosome(this.getTimeSeries);
    first.lSystem = LSysGenerator.generateRandomLSystem();
    first.value = this.value;
    first.dValue = other.dValue;

    const second = new Chromosome(this.getTimeSeries);
    second.lSystem = LSysGenerator.generateRandomLSystem();
    second.value = other.value;
    second.dValue = this.dValue;

    first.calculateFitness();
    second.calculateFitness();

    return [first, second];
  }

  calculateFitness(): void {
    const originTimeSeries = this.getTimeSeries();

    const sequence = new SequenceConstructor().setConfiguration({
      symbolsCount: originTimeSeries.length,
      lSystem: this.lSystem
    }).build();

    const timeSeries = new TimeSeriesConstructor().setConfiguration({
      sequence,
      value: this.value,
      dValue: this.dValue
    }).build();

    let fitness = 0;
    for (let i = 0; i < originTimeSeries.length; i++) {
      fitness += (originTimeSeries[i] - timeSeries[i]) ** 2;
    }
    this.fitness = fitness;
  }

  deepcopy(): Chromosome{
    const clone = new Chromosome(this.getTimeSeries);
    clone.value = this.value;
    clone.dValue = this.dValue;
    clone.lSystem = this.lSystem.deepcopy();
    return clone;
  }

  toString(): string {
    return `${this.lSystem.toString()} ${this.value} ${this.dValue}`;
  }
}

let gnetic: GNetic = null;

const pipe = buildWorkerParentEndpoint(parentPort);

pipe.onMessage(GNeticMessageTypes.BEGIN, (config: GNeticConfig ) => {
  gnetic = new GNetic(config);
  gnetic.run();
  gnetic.onSolutionFound(solution => {
    pipe.sendMessage(GNeticMessageTypes.SOLUTION_FOUND, solution);
  })
})

pipe.onMessage(GNeticMessageTypes.STOP, () => {
  gnetic.stop();
  gnetic = null;
})

pipe.onRequest(GNeticMessageTypes.GIVE_INTERMEDIATE_RESULTS, () => {
  return gnetic.getBestChromosomes();
})
