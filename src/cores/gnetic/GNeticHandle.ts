import { LSysDescription } from '../gmatic/LSysDescription';
import { GNeticMessageTypes } from './constants';
import path from 'path'; 
import { buildWorkerEndpoint } from '../messages/worker_threads/Threads';
import { PipeEndpoint } from '../messages/pipe/PipeEndpoint';


export class GNeticConfig {
  public timeSeries: Array<Number>;
  public surviveCount: number;
  public crossoveringCount: number;
  public mutationCount: number;
  public accuracy: number;
}


export class GNeticHandle {
  private worker: PipeEndpoint = null;
  constructor(){
    
  }

  public onSolutionFound(callback: (lsys: LSysDescription, value: number, dValue: number) => void){
    this.worker.onMessage(GNeticMessageTypes.SOLUTION_FOUND, callback);
  }

  public async run(config: GNeticConfig){
    this.worker = buildWorkerEndpoint(path.join(__dirname, 'GNeticCore.js'));
    this.worker.sendMessage(GNeticMessageTypes.BEGIN, config);
  }
  public async getCurrentResults(){
    return await this.worker.sendRequest(GNeticMessageTypes.GIVE_INTERMEDIATE_RESULTS, null);
  }
  public async stop(){
    this.worker.sendMessage(GNeticMessageTypes.STOP, null);
  }
}

