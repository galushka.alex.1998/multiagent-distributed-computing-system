
export class TypedMessage {
  public type: string;
  public payload: any;

  constructor(type: string, payload: any){
    this.type = type;
    this.payload = payload;
  }
}

export class AsyncMessage extends TypedMessage{
  public correlationId: string;

  constructor(type: string, correlationId: string, payload: any){
    super(type, payload);
    this.correlationId = correlationId;
  }
}

export enum TransferType {
  request = 'request',
  response = 'request',
  message = 'message',
}

export class TransferMessage extends AsyncMessage {
  public transferType: TransferType;

  constructor(transferType: TransferType, type: string, correlationId: string, payload: any){
    super(type, correlationId, payload);
    this.transferType = transferType;
  }
}