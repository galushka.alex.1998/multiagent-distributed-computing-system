import { Worker, parentPort } from 'worker_threads';
import { IPipeable } from '../pipe/IPipeable';
import { PipeEndpoint } from '../pipe/PipeEndpoint';


export class ChildThread implements IPipeable{
  private worker: Worker;

  constructor(path: string){
    this.worker = new Worker(path); 
  }
  onMessage(callback: (...args) => void): void {
    this.worker.on('message', callback);  
  }
  sendMessage(message: any): void {
    this.worker.postMessage(message);
  }
  onError(errback: (...args) => void): void {
    this.worker.on('messageerror', errback);
    this.worker.on('error', errback);
  }
  async terminate(){
    await this.worker.terminate()
  }
}

export class ParentThread implements IPipeable {
  private parentPort;
  constructor(parentPort){
    this.parentPort = parentPort;
  }
  onMessage(callback: (...args) => void): void {
    this.parentPort.on('message', callback);
  }
  sendMessage(message: any): void {
    this.parentPort.postMessage(message);
  }
  onError(errback: (...args) => void): void {
    this.parentPort.on('messageerror', errback);
    this.parentPort.on('error', errback);
  }
  async terminate(){
    
  }
}


export function buildWorkerParentEndpoint(parentPort): PipeEndpoint {
  const pipe = new ParentThread(parentPort);
  const endPoint = new PipeEndpoint(pipe);

  return endPoint;
}


export function buildWorkerEndpoint(workerPath) : PipeEndpoint {
  const pipe = new ChildThread(workerPath);
  const endPoint = new PipeEndpoint(pipe);
  return endPoint;
}