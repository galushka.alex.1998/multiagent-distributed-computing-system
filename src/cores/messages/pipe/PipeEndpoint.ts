import { IPipeable } from "./IPipeable";
import { EventEmitter } from 'events';
import { TransferMessage, TransferType } from "../AsyncMessage";
import { uniqueId } from 'lodash'


export class PipeEndpoint extends EventEmitter {
  private pipe: IPipeable;
  constructor(pipe: IPipeable){
    super();
    this.pipe = pipe;
    this.pipe.onMessage((message: TransferMessage) => 
      this.emit(message.transferType.concat(message.type), message)
    );
  } 
  onMessage(type: string, callback: (...args) => void){
    this.on(TransferType.message.concat(type), (message: TransferMessage) => callback(message.payload));
  }

  onRequest(type: string, callback: (...args) => any){
    this.on(TransferType.request.concat(type), async (message: TransferMessage) => {
      const res = await callback(message.payload);
      const content: TransferMessage = new TransferMessage(
        TransferType.response, 
        type, 
        message.correlationId, 
        res
      );
      this.pipe.sendMessage(content);
    })
  }

  sendMessage(type: string, message: any){
    const content: TransferMessage = new TransferMessage(
      TransferType.message, 
      type, 
      null, 
      message
    );

    this.pipe.sendMessage(content);
  }

  async sendRequest(type: string, message: any): Promise<any> {
    const content: TransferMessage = new TransferMessage(
      TransferType.request, 
      type, 
      uniqueId(), 
      message
    );
    this.pipe.sendMessage(content);
    return await new Promise((resolve, reject) => {
      const timeOut = setTimeout(reject, 3000);
      const receiveResponse = (message: TransferMessage) => {
        if (message.correlationId === content.correlationId){
          clearTimeout(timeOut);
          this.removeListener(TransferType.response.concat(type), receiveResponse);
          resolve(message.payload);
        }
      }
      this.on(TransferType.response.concat(type), receiveResponse);
    });
  }

}