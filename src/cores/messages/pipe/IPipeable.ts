

export interface IPipeable {
  onMessage(callback: Function): void;
  sendMessage(message: any): void;
  onError(errback: Function): void;
  terminate(): void
}
