

export interface IConstructor<TConfig, TOutput>{
  setConfiguration(config: TConfig): IConstructor<TConfig, TOutput>;
  build(): TOutput;
}