import { IConstructor } from "./IConstructor";


export class TimeSeriesConstructorConfig {
  public sequence: string;
  public value: number;
  public dValue: number;
}

export class TimeSeriesConstructor implements IConstructor<TimeSeriesConstructorConfig, Array<number>> {
  private config: TimeSeriesConstructorConfig;
  setConfiguration(config: TimeSeriesConstructorConfig){
    this.config = config;
    return this;
  }
  build(): number[] {
    let currentValue = this.config.value;
    let dValue = this.config.dValue;
    const timeSeries: Array<number> = [];
    Array.from(this.config.sequence).forEach((symbol) => {
      switch (symbol) {
        case "f":
        case "g":
        case "h":
          timeSeries.push(currentValue);
          break;
        case "+":
          currentValue += dValue;
          break;
        case "-":
          currentValue -= dValue;
          break;
      }
    });
    return timeSeries;
  }
  
}