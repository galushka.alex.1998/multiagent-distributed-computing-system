import { LSys } from "../cores/gmatic/LSys";
import { IConstructor } from "./IConstructor";


export class SequenceConstructorConfig {
  public symbolsCount: number;
  public lSystem: LSys;
}

export class SequenceConstructor implements IConstructor<SequenceConstructorConfig, string>{
  private config: SequenceConstructorConfig = null;
  setConfiguration(config: SequenceConstructorConfig): SequenceConstructor {
    this.config = config;
    return this;
  }
  build(): string {
    const lsys = this.config.lSystem;
    let start = lsys.axiomRule.production;
    while (
      Array.from(start).filter((s) =>
        lsys.config.significant.some((symbol) => symbol === s)
      ).length < this.config.symbolsCount
    ) {
      start = lsys.iterate(start);
    }

    let result = "";
    let sigCount = 0;
    for (let i = 0; sigCount < this.config.symbolsCount; i++) {
      result = result + start[i];
      if (lsys.config.significant.some((s) => s === start[i])) {
        sigCount++;
      }
    }
    return result;
  }
  
}